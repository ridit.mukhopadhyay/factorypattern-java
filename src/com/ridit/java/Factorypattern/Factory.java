package com.ridit.java.Factorypattern;

public class Factory {
	
	public static Food makeFood(String type){
		
		if(type.equals("ch")) {
			return new Chocolate();
		}
		if(type.equals("bi")) {
			return new Biscuit();
		}
		
		return null;
		
		
		
		
	}

}


